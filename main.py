import MySQLdb
import os
import urllib2
from bs4 import BeautifulSoup

host = os.environ['mysql_host']
user = os.environ['mysql_user']
passwd = os.environ['mysql_pass']

db = MySQLdb.connect(host,user,passwd,"licitacoes")

cursor = db.cursor()
cursor.execute("select id, base_url, main_page from cidades where ativo = 1")
cursor.fetchall()

for cidade in cursor:
    cidade_id = cidade[0]
    quote_page = cidade[1]
    main_page_url = cidade[2]
    
    i = 0
    while True:
        i += 1
        urls = []
        print("cidade " + str(cidade_id) + ", pagina " + str(i))
        try:
            page = urllib2.urlopen(quote_page + str(i))
        except:
            break
        else:
            soup = BeautifulSoup(page, 'html.parser')

            if cidade_id == 1:
                ## Frutal implementation
                links = soup.find_all('div', class_='bp-head')
                if len(links) == 0:
                    break

                for h in links:
                    a = h.find('a')            
                    urls.append(a.attrs['href'])

            if cidade_id == 2:
                ## Itapagipe implementation
                links = soup.find_all('a', class_='btn btn-plenus btn-sm')
                if len(links) == 0:
                    break
                    
                for h in links:
                    urls.append(main_page_url + h.attrs['href'])

            if len(urls) > 0:
                insertQuery = "insert ignore into licitacoes (cidade_id, url) values "
                for link in urls:
                    insertQuery = insertQuery + "(" + str(cidade_id) + ", '" + link + "'), "
                insertQuery = insertQuery[0 : len(insertQuery) - 2]

                t = cursor.execute(insertQuery)
                db.commit()
                
                if t == 0:
                    break

db.close()